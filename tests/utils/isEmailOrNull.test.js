import { expect } from 'chai';
import { describe, it } from 'mocha';
import sequelize from 'sequelize';
import isEmailOrNullUtil from '../../src/data/utils/isEmailOrNull';

describe('utils / isEmailOrNullUtil', () => {
  it('Should have be a function', () => {
    expect(isEmailOrNullUtil).to.be.an('function');
  });

  const badValues = [
    'notaEmail',
    -1,
    {},
    'almost.an@email',
  ];

  const badValuesTest = value => expect(() => isEmailOrNullUtil(sequelize.Validator, value)).to.throw();

  it('Should throw exception', () => {
    badValues.forEach(badValuesTest);
  });

  it('Should validate properly', () => {
    expect(isEmailOrNullUtil(sequelize.Validator, 'very@good.email')).to.equal(true);
    expect(isEmailOrNullUtil(sequelize.Validator, '')).to.be.equal(true);
  });
});
