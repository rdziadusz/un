import { expect } from 'chai';
import { describe, it } from 'mocha';
import resolvers from '../src/data/resolvers';

describe('Resolvers', () => {
  it('Should return a an object', () => {
    expect(resolvers).to.be.an('object');
  });

  it('Should return contain Query, Team and User fields', () => {
    expect(resolvers).to.have.all.keys('Query', 'Team', 'User');
  });
});
