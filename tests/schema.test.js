import { expect } from 'chai';
import { describe, it } from 'mocha';
import { GraphQLInt, GraphQLString } from 'graphql';
import schema from '../src/data/schema';

describe('Shema', () => {
  it('Should be a member of class GraphQLSchema', () => {
    expect(schema.constructor.name).to.equal('GraphQLSchema');
  });
  it('Should have two elements', () => {
    expect(schema.getType('User')).to.be.an('object');
    expect(schema.getType('Team')).to.be.an('object');
  });
});

describe('User', () => {
  const User = schema.getType('User');
  const Team = schema.getType('Team');

  it('Should have a type GraphQLObjectType', () => {
    expect(User.constructor.name).to.equal('GraphQLObjectType');
  });
  it('Shoud have six fields', () => {
    expect(User.getFields()).to.have.all.keys('id', 'team', 'email', 'firstName', 'lastName', 'roleDescription');
  });

  const UserExpectedFields = [
    { field: 'id', classType: GraphQLInt },
    { field: 'team', classType: Team },
    { field: 'email', classType: GraphQLString },
    { field: 'firstName', classType: GraphQLString },
    { field: 'lastName', classType: GraphQLString },
    { field: 'roleDescription', classType: GraphQLString },
    { field: 'firstName', classType: GraphQLString },
  ];

  const fieldKeysTest = (expectedField) => {
    expect(User.getFields()[expectedField.field], expectedField.field).to.be.an('object');
    expect(User.getFields()[expectedField.field].type, `type of ${expectedField.field}`).to.equal(expectedField.classType);
    expect(User.getFields()[expectedField.field].description, `key ${expectedField.field}`).to.not.be.empty;
  };

  it('Should have proper fields configuration', () => {
    UserExpectedFields.forEach(fieldKeysTest);
  });
});

describe('Team', () => {
  const Team = schema.getType('Team');

  it('Should have a type GraphQLObjectType', () => {
    expect(Team.constructor.name).to.equal('GraphQLObjectType');
  });
  it('Shoud have three fields', () => {
    expect(Team.getFields()).to.have.all.keys('name', 'user');
  });

  it('Should have proper fields configuration', () => {
    expect(Team.getFields().name, 'name').to.be.an('object');
    expect(Team.getFields().name.type, 'type of name').to.equal(GraphQLString);
    expect(Team.getFields().name.description, 'key name').to.not.be.empty;

    expect(Team.getFields().user, 'user').to.be.an('object');
  });
});
