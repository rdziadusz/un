import { expect } from 'chai';
import { describe, it } from 'mocha';
import query from '../../src/data/queries/rootQuery';

describe('queries / Query', () => {
  it('Should have be a function', () => {
    expect(query).to.be.an('function');
  });


  it('Should have return an single element Array', () => {
    expect(query()).to.be.a('Array');
    expect(query()).to.not.be.empty;
    expect(query()).to.have.lengthOf(1);
  });

  it('Should contain at least information about teams and users', () => {
    expect(query()[0]).to.have.string('users');
    expect(query()[0]).to.have.string('[User]');
    expect(query()[0]).to.have.string('teams');
    expect(query()[0]).to.have.string('[Team]');
  });
});