import { expect } from 'chai';
import { describe, it } from 'mocha';
import Team from '../../src/data/resolvers/team';

describe('resolvers / Team', () => {
  it('Should return a an object', () => {
    expect(Team).to.be.an('object');
  });

  it('Should return contain Query, Team and User fields', () => {
    expect(Team).to.have.all.keys('user');
  });
});
