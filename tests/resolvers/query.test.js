import { expect } from 'chai';
import { describe, it } from 'mocha';
import Query from '../../src/data/resolvers/query';

describe('resolvers / Query', () => {
  it('Should return a an object', () => {
    expect(Query).to.be.an('object');
  });

  it('Should return contain Query, Team and User fields', () => {
    expect(Query).to.have.all.keys('users', 'teams');
  });
});
