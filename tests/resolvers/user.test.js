import { expect } from 'chai';
import { describe, it } from 'mocha';
import User from '../../src/data/resolvers/user';

describe('resolvers / User', () => {
  it('Should return a an object', () => {
    expect(User).to.be.an('object');
  });

  it('Should return contain Query, Team and User fields', () => {
    expect(User).to.have.all.keys('team');
  });
});
