import { expect } from 'chai';
import { describe, it } from 'mocha';
import SequalizeMock from 'sequelize-mock';
import user from '../../src/data/models/user';

const DBConnectionMock = new SequalizeMock();
const User = user(DBConnectionMock, SequalizeMock);

describe('models / user', () => {
  it('Should create new user', (done) => {
    const testEmail = 'test@email.com';

    User.create({
      email: testEmail,
    }).then((Created) => {
      expect(Created.email).to.equal(testEmail);
      done();
    });
  });
});
