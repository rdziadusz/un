import { expect } from 'chai';
import { describe, it } from 'mocha';
import SequalizeMock from 'sequelize-mock';
import team from '../../src/data/models/team';

const DBConnectionMock = new SequalizeMock();
const Team = team(DBConnectionMock, SequalizeMock);

describe('models / team', () => {
  it('Should create new team', (done) => {
    const teamTestName = 'testName';
    Team.create({
      name: teamTestName,
    }).then((teamCreated) => {
      expect(teamCreated.name).to.equal(teamTestName);
      expect(teamCreated.id).to.be.an('number');
      expect(teamCreated.id).to.equal(1);
      done();
    });
  });

  it('Should create new team without name', (done) => {
    Team.drop();
    const teamTestName = {};
    Team.create({
      name: teamTestName,
      noexistingFields: -1,
    }).then((teamCreated) => {
      console.log(teamCreated);
      expect(teamCreated.name).to.equal(teamTestName);
      expect(teamCreated.id).to.be.an('number');
      done();
    });
  });
});
