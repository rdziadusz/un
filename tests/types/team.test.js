import { expect } from 'chai';
import { describe, it } from 'mocha';
import teamType from '../../src/data/types/team';

describe('types / Team', () => {
  it('Should have be a function', () => {
    expect(teamType).to.be.an('function');
  });

  it('Should have return an Array', () => {
    expect(teamType()).to.be.a('Array');
    expect(teamType()).to.not.be.empty;
  });
});
