import { expect } from 'chai';
import { describe, it } from 'mocha';
import userType from '../../src/data/types/user';

describe('types / User', () => {
  it('Should be a function', () => {
    expect(userType).to.be.an('function');
  });

  it('Should return an Array', () => {
    expect(userType()).to.be.a('Array');
    expect(userType()).to.not.be.empty;
  });
});
