import multer from 'multer';

export default {
  storage: multer.memoryStorage(),
  fileFilter: (req, file, cb) => {
    if (!file.originalname.match(/\.(csv|CSV)$/)) {
      return cb(new Error('Only csv files are allowed!'), false);
    }

    cb(null, true);
  },
};
