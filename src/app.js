import express from 'express';
import graphqlHTTP from 'express-graphql';
import multer from 'multer';
import schema from './data/schema';
import importer from './data/importer';
import db from './data/db';
import multerConfig from './config/multer';

const app = express();

app.use('/graphql', graphqlHTTP(() => ({
  schema,
  graphiql: app.get('env') === 'development',
  livereload: true,
})));

app.post('/upload', multer(multerConfig).single('users'), (req, res, next) => {
  importer(req.file.buffer).then(() => {
    res.status(200).send('File loaded');
  }, (rejection) => {
    console.error('rejected', rejection);
    throw new Error(rejection);
  }).catch((err) => {
    next(err);
  });
}, (err, req, res, next) => {
  res.status(err.status || 500);
  res.send({
    message: err.message,
    error: app.get('env') === 'development' ? err : {},
  });
});

process.on('uncaughtException', (err) => {
  console.error(err);
});

app.get('/', (req, res) => {
  db.Team.sync().then(() => {
    db.User.sync();
  });
  res.send('Db reloded');
});

app.listen(3001, () => {
  console.info('Listening on port 3001!');
});
