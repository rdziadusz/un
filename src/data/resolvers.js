import QueryResolver from './resolvers/query';
import TeamResolver from './resolvers/team';
import UserResolver from './resolvers/user';

export default {
  Query: QueryResolver,
  Team: TeamResolver,
  User: UserResolver,
};
