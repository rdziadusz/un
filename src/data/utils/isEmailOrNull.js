/**
 * @param validator
 * validator should have pre implemented functions isNull and isEmail
 * @throws 'Require either empty or Email'
 */
export default (validator, email) => {
  if (!validator.isNull(email) && !validator.isEmail(email)) {
    throw new Error('Require either empty or Email');
  }

  return true;
};
