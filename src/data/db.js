import Sequelize from 'sequelize';
import config from '../config/db';
import user from './models/user';
import team from './models/team';

const sequelize = new Sequelize(config.name, config.user, config.password, {
  host: config.host,
  dialect: 'mysql',
  port: config.port,
  operatorsAliases: false,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 2000,
    handleDisconects: true,
  },
  define: {
    charset: 'utf8',
    collate: 'utf8_general_ci',
  },
});

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.User = user(sequelize, Sequelize);
db.Team = team(sequelize, Sequelize);
db.Team.User = db.Team.hasMany(db.User);
db.User.Team = db.User.belongsTo(db.Team);

module.exports = db;
