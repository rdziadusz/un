import Team from './team';

const User = `
    type User {
        #Id number
        id: Int
        #Role desciption
        roleDescription: String,
        # Usern name
        firstName: String,
        # User surname
        lastName: String,
        # User unique email
        email: String
        # Team which User belongs to
        team: Team
    }
`;

export default () => [Team, User];
