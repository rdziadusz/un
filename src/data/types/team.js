import User from './user';

const Team = `
    type Team {
        # Team name
        name: String
        # User list of the team
        user: [User]
    }
`;

export default () => [Team, User];
