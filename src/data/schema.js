import { makeExecutableSchema } from 'graphql-tools';
import Team from './types/team';
import User from './types/user';
import RootQuery from './queries/rootQuery';
import Resolvers from './resolvers';

export default makeExecutableSchema({
  typeDefs: [
    RootQuery,
    User,
    Team,
  ],
  resolvers: Resolvers,
});
