const RootQuery = `
  type Query {
    users: [User],
    teams: [Team]
  }
`;

export default () => [RootQuery];
