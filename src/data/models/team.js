export default (sequelize, DataTypes) => sequelize.define('team', {
  name: {
    unique: true,
    type: DataTypes.STRING,
  },
});
