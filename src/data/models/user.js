import isEmailOrNullUtil from '../utils/isEmailOrNull';

export default (sequelize, DataTypes) => sequelize.define('user', {
  firstName: DataTypes.STRING,
  lastName: DataTypes.STRING,
  roleDescription: DataTypes.STRING,
  email: {
    type: DataTypes.STRING,
    unique: true,
    allowNull: true,
    validate: {
      isEmailOrNull() {
        isEmailOrNullUtil(sequelize.Validator, this.email);
      },
    },
  },
  teamId: {
    type: DataTypes.INTEGER,
    foreignKey: true,
    references: {
      model: 'teams',
      key: 'id',
    },
  },
});
