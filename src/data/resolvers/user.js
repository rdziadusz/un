import { resolver } from 'graphql-sequelize';
import db from './../db';

export default {
  team: resolver(db.User.Team),
};
