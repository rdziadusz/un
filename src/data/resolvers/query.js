import { resolver } from 'graphql-sequelize';
import db from './../db';

export default {
  teams: resolver(db.Team),
  users: resolver(db.User),
};
