import stream from 'stream';
import parse from 'csv-parse';
import db from './db';

const importer = async (buffer) => {
  const readable = new stream.Readable();
  readable.push(buffer);
  readable.push(null);
  readable.pipe(parse({ from: 2, skip_empty_lines: true }))
    .on('data', (data) => {
      db.Team.findOrCreate({
        where: {
          name: data[4],
        },
      }).spread(team => team.get({
        plain: true,
      })).then(team => db.User.upsert({
        firstName: data[0],
        lastName: data[1],
        email: data[2] || null,
        roleDescription: data[3],
        teamId: team.id,
      })).catch((err) => {
        console.error('Field error', err);
      });
    })
    .on('error', (err) => {
      console.error('Stream Error', err);
      throw new Error('Stream Error');
    }).on('finish', (err) => {
      console.error('Stream Finish', err);
    });
};

export default importer;
