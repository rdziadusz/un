import db from './src/data/db';

const reloadDb = async () => {
  await db.sequelize.drop();
  await db.Team.sync();
  await db.User.sync();
};

reloadDb().then(() => {
  console.info('Database reloaded');
}, (err) => {
  console.error('Error', err);
}).finally(() => process.exit());
