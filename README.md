# CSV Importer
Application for importing csv files using stream api and graphQL for serving data
## Instalation

### Requirements
	node 8.9.4
	npm 5.6.0
	mysql 5.7
	open port 3301
Tested on `Fedora 27`
### Instalation
	git clone <url> project
	cd project
	npm install
### First usage and usefull scripts
Follow these steps:
```
 - vi src/config/db.js // config mysql database connection
 - npm run db-reload // setup databases
 - npm test // run test
 - npm run production // run application in production mode
```

For development / **graphqli** usage application can be run by **`npm start`**

#### **Disclaimer**
Technically is it possible to cennect to any other type of database by changing file ``src/db.js`` at field ``dialect``. This operation is highy unrecommended, as it was only briefly tested.

## Resources
### Applcation resolves two kinds of resources 

 - Rest API for flat file handling
 - GraphQL API for data retrival
### Resources list
----------
GET|POST: `http://localhost:3301/graphql` Standard `graphQL`  API for requests
**curl example**: `curl -X POST \
  'http://localhost:3001/graphql?query=%7B%0A%20%20users%20%7B%0A%20%20%20%20id%0A%20%20%20%20firstName%0A%20%20%20%20roleDescription%0A%20%20%20%20email%0A%20%20%20%20team%20%7B%0A%20%20%20%20%20%20name%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D%0A' \
  -H 'Cache-Control: no-cache' \
  -H 'Postman-Token: 33fdce22-bc70-4964-a2ed-747317f67edb' \
  -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'`

----------

GET: `http://localhost:3301/graphql` Only in **development mode** for query visualization
    
----------


   POST: `http://localhost:3001/upload`  for uploading file with strict field name `users` and extension `csv`
   **curl example**: `curl -X POST \
  http://localhost:3001/upload \
  -H 'Cache-Control: no-cache' \
  -H 'Postman-Token: 97de9008-0102-4503-b978-2e42191b7111' \
  -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
  -F users=@/users.csv`


## General information
Application is writen using `ESLint` with `airbnb-base` extension. It uses `ES6` features, which are decoded using `Babel` with `es2015` preset. 
For data handling I'm using `Sequelize` orm, which handles customly implemented data fetch from nativ `Node.js` with `stream API`

Apication uses standard `graphQL` directory structure with `Sequelize` models
```

 - data
	 - queries //graphQL
	 - resolvers //graphQL
	 - types //graphQL
	 - models //models are used for db orm

```
Main script is `app.js` in `src` directory

------
Tests are based on `mocha-chai` stack with mockup tools: `sequelize-mock`